package es.us.isa.bpms;

/**
 * Clase de las excepciones que lanza el paquete bpms. 
 * 
 * @author Edelia Garcia Gonzalez
 * @version 1.0
 *
 */
@SuppressWarnings("serial")
public class BpmsException extends Exception {

	public BpmsException(String message) {
		
		super(message);
	}

}
